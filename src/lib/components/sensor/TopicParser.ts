export function getTopic(topics: any, topicPlaceholder: string): string {
    const levels: string[] = topicPlaceholder.split(".");
    let subObject = topics;
    
    for (let i = 0; i < levels.length; i++) {
        subObject = subObject[levels[i]];
    }

    return subObject;
}