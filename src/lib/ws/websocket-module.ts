import type { Payload } from "$lib/model/payload/payload";
import { Action } from "$lib/model/ws/action";
import type { Priority } from "$lib/model/ws/priority";
import type { PublishMessage } from "$lib/model/ws/publish-message";
import type { SubscribeMessage } from "$lib/model/ws/subscribe-message";
import type { SubscribeRequest } from "$lib/model/ws/subscribe-request";

export default class WebSocketModule {
    private connection: WebSocket;
    private subscriptions: Map<string, ((message: PublishMessage) => void)[]>;
    private subscribeRequests: SubscribeRequest[];
    private publishRequests: PublishMessage[];

    constructor(url: string, port?: number) {
        this.connection = new WebSocket(
            `ws://${url}${port === undefined ? "" : `:${port}`}`
        );
        this.subscriptions = new Map();
        this.subscribeRequests = [];
        this.publishRequests = [];

        this.connection.onopen = () => {
            this.subscribeRequests.forEach(subscribeRequest => {
                this.subscribe(subscribeRequest.topic, subscribeRequest.callback);
            });
            this.publishRequests.forEach(publishRequest => {
                this.publishMessage(publishRequest);
            });
        };

        this.connection.onmessage = (event: MessageEvent<string>) => {
            const message: PublishMessage = JSON.parse(event.data);
            const callbacks: ((message: PublishMessage) => void)[] | undefined = this.subscriptions.get(message.topic);

            if (callbacks !== undefined) {
                callbacks.forEach(callback => {
                    callback(message);
                });
            }
        };

        this.connection.onerror = () => {
            console.error(`An error ocurred on WSS connection`);
        };

        this.connection.onclose = () => {
            console.log("WebSocket connection closed");
        };
    }

    subscribe(topic: string, callback: (message: PublishMessage) => void) {
        if (this.isOpen()) {
            const callbacks = this.subscriptions.get(topic);

            if (callbacks !== undefined) {
                callbacks.push(callback);
            } else {
                const message: SubscribeMessage = {
                    "action": Action.SUBSCRIBE,
                    "topic": topic
                }

                this.subscriptions.set(topic, [callback]);
                this.connection.send(
                    JSON.stringify(message)
                );
            }
        } else {
            this.subscribeRequests.push({topic, callback});
        }
    }

    publish(topic: string, priority: Priority, payload: Payload) {
        const message: PublishMessage = {
            "action": Action.PUBLISH,
            "topic": topic,
            "payload": payload,
            "priority": priority
        }

        this.publishMessage(message);
    }

    private publishMessage(message: PublishMessage) {
        if (this.isOpen()) {
            this.connection.send(
                JSON.stringify(message)
            );
        } else {
            this.publishRequests.push(message);
        }
    }

    isOpen() {
        return this.connection.readyState === 1;
    }

    close() {
        this.connection.close(1000);
    }
}