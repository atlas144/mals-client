import { writable, type Writable } from 'svelte/store';
import type { MapPointModel } from '../model/mapPoint/map-point';

export const path: Writable<MapPointModel[]> = writable([
    {
        lat: 48.97766,
        lon: 14.44573
    },
    {
        lat: 48.9777392,
        lon: 14.4461089
    }
]);
export const running: Writable<boolean> = writable(false);
export const paused: Writable<boolean> = writable(false);