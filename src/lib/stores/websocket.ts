import { browser } from '$app/environment';
import WebSocketModule from '$lib/ws/websocket-module';
import { readable } from 'svelte/store';

export const webSocketModule = readable<WebSocketModule>(undefined, function start(set) {
	let webSocketModule: WebSocketModule;
	
	if (browser) {
		webSocketModule = new WebSocketModule(location.hostname, 14400);

		set(webSocketModule);
	}

	return function stop() {
		if (browser) {
			webSocketModule.close();
		}
	};
});