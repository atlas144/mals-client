import { writable, type Writable } from 'svelte/store';

export const speed: Writable<number> = writable(0);
export const threshold: Writable<number> = writable(100);
export const running: Writable<boolean> = writable(false);
export const onPath: Writable<boolean> = writable(false);