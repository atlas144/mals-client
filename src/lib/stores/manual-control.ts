import { writable, type Writable } from 'svelte/store';

export const speed: Writable<number> = writable(0);