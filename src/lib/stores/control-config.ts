import { ControlType } from '$lib/model/settings/control-type';
import { writable, type Writable } from 'svelte/store';

export const controlType: Writable<ControlType> = writable(ControlType.MANUAL);