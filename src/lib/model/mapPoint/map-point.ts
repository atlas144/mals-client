export interface MapPointModel {
    lat: number,
    lon: number
}