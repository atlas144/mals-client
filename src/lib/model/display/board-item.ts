export interface BoardItem {
    x: number,
    y: number,
    w: number,
    h: number,
    fixed?: boolean,
    resizable?: boolean,
    draggable?: boolean,
    min?: {
        w?: number,
        h?: number,
    },
    max?: {
        w?: number,
        h?: number,
    },
    customDragger?: boolean,
    customResizer?: boolean,
}