export interface ChartData {
    labels: string[];
    datasets: {
        name?: string,
        values: number[];
    }[];
}