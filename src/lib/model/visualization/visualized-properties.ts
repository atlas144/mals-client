export default interface VisualizedProperties {
    ssaFL: {
        front: boolean,
        bottom: boolean
    }, ssaFR: {
        front: boolean,
        bottom: boolean
    }, ssaBL: {
        front: boolean,
        bottom: boolean
    }, ssaBR: {
        front: boolean,
        bottom: boolean
    }, lidar: boolean,
    tactileF: boolean,
    tactileB: boolean
}