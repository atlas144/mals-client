export enum Direction {
    FORWARD = "FORWARD",
    RIGHT = "RIGHT",
    BACKWARD = "BACKWARD",
    LEFT = "LEFT"
}