import type { Payload } from "$lib/model/payload/payload";

export interface LidarPayload extends Payload {
    distance: number,
    angle: number
}