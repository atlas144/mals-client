import type { Sensor } from "$lib/model/sensor/sensor"

export interface TactileModel extends Sensor {}