import type { BoardItem } from "$lib/model/display/board-item"
import type { Metadata } from "./metadata"

export interface Sensor {
    metadata: Metadata,
    boardItem: BoardItem,
}