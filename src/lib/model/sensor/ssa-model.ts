import type { Sensor } from "$lib/model/sensor/sensor"

export interface SsaModel extends Sensor {
    measurements: number
}