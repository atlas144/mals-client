import type { ModuleCategory } from "./module-category";
import type { ModuleType } from "./module-type";

export interface Metadata {
    id: number,
    name: string,
    category: ModuleCategory,
    type: ModuleType,
    topics: {
        [key: string]: string;
    }
}