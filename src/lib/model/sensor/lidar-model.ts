import type { Sensor } from "$lib/model/sensor/sensor"

export interface LidarModel extends Sensor {
    angleHalf: number
}