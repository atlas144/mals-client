// @ts-ignore
import Lidar from "$lib/components/sensor/Lidar.svelte";
import Ssa from "$lib/components/sensor/Ssa.svelte";
import Tactile from "$lib/components/sensor/Tactile.svelte";

export class ModuleType {
    static readonly SSA = new ModuleType("SSA", Ssa);
    static readonly LIDAR = new ModuleType("LIDAR", Lidar);
    static readonly TACTILE = new ModuleType("TACTILE", Tactile);

    private readonly moduleName: string;
  
    private constructor(moduleName: string, readonly component: any) {
        this.moduleName = moduleName;
    }

    static valueOf(moduleName: string): ModuleType {
        for (const [entryName, module] of Object.entries(ModuleType)) {
            if (entryName === moduleName) {
                return module;
            }
        };

        throw new Error(`Module name '${moduleName}' does not belong to any registered module!`);
    }
}