import ManualControl from "$lib/components/control/ManualControl.svelte";
import LtControl from "$lib/components/control/LtControl.svelte";
import AutonomousControl from "$lib/components/control/AutonomousControl.svelte";

export class ControlType {
    static readonly MANUAL = new ControlType("Manual", ManualControl);
    static readonly LineTracking = new ControlType("Line tracking", LtControl);
    static readonly AUTONOMOUS = new ControlType("Autonomous", AutonomousControl);

    readonly text: string;
  
    private constructor(text: string, readonly component: any) {
        this.text = text;
    }
}