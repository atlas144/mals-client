export enum Action {
    PUBLISH = "PUBLISH",
    SUBSCRIBE = "SUBSCRIBE"
}