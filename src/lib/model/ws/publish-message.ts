import type { Payload } from "$lib/model/payload"
import type { Message } from "./message"
import type { Priority } from "./priority"

export interface PublishMessage extends Message {
    topic: string,
    payload: Payload
    priority: Priority
}