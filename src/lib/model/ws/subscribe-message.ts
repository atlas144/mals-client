import type { Message } from "./message";

export interface SubscribeMessage extends Message {
    topic: string
}