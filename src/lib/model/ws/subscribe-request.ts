import type { PublishMessage } from "./publish-message";

export interface SubscribeRequest {
    topic: string,
    callback: (message: PublishMessage) => void
}