import type { Action } from "./action";

export interface Message {
    action: Action,
}